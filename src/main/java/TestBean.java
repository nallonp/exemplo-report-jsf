import net.sf.jasperreports.engine.JasperRunManager;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@ManagedBean(name = "testBean")
@SessionScoped
public class TestBean {
    public void download() throws SQLException {

        Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).info("Gerando arquivo!");
        try (Connection connection = ConnectionFactory.getWildFlyConnection()) {
            // String sourceFileName = "/tmp/test.jasper";
            FacesContext context = FacesContext.getCurrentInstance();
            context.responseComplete();
            ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
            String sourceFileName = servletContext.getRealPath("/WEB-INF/reports/test.jasper");
            System.out.println(sourceFileName);
            System.out.println(connection.isValid(5) ? "Conexão válida" : "Conexão inválida");
            System.out.println(new File(sourceFileName).isFile());
            Map<String, Object> params = new HashMap<>();
            params.put("TITLE", "FORMULÁRIO TESTE");
            byte[] bytes = JasperRunManager.runReportToPdf(sourceFileName, params, connection);

            //Download
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();

            ec.responseReset();
            ec.setResponseContentType("application/pdf");
            ec.setResponseContentLength(bytes.length);
            String fileName = "relatorio666.pdf";
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            response.getOutputStream().write(bytes);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            fc.responseComplete();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
