import net.sf.jasperreports.engine.JasperRunManager;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        try (Connection connection = ConnectionFactory.getConnection()) {
            //String sourceFileName = "/home/nallon/Área de Trabalho/exemplo-report-jsf/src/reports/test.jasper";
            FacesContext context = FacesContext.getCurrentInstance();
            context.responseComplete();
            ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
            String sourceFileName = servletContext.getRealPath("/WEB-INF/reports/test.jasper");
            System.out.println(sourceFileName);
            System.out.println(new File(sourceFileName).isFile());
            Map<String, Object> params = new HashMap<>();
            params.put("TITLE", "FORMULÁRIO TESTE");
            byte[] bytes = JasperRunManager.runReportToPdf(sourceFileName, params, connection);
            // byte[] bytes = JasperRunManager.runReportToPdf(sourceFileName, params, connection);
            // System.out.println(bytes.length);
          /*  JasperPrint jasperPrint = JasperFillManager.fillReport(
                    sourceFileName, params, connection);
            JasperExportManager.exportReportToPdfFile(
                    jasperPrint, "/tmp/simple_report.pdf");*/

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
