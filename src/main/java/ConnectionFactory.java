import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static final String url = "jdbc:postgresql://127.0.0.1:5432/cursos_alura";
    private static final String user = "nallon";
    private static final String password = "slj00237!";


    public static Connection getWildFlyConnection() throws Exception {
        try {
            InitialContext ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:/jdbc/test_db");
            return ds.getConnection();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public static Connection getConnection() throws Exception {
        Connection conn = null;
        Class.forName("org.postgresql.Driver");
        try {
            conn = DriverManager.getConnection(url, user, password);
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }

}
